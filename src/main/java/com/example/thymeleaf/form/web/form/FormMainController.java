package com.example.thymeleaf.form.web.form;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FormMainController {

    @GetMapping("/form/main")
    public String fromMain(){
        return "form/index";
    }
}
